/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require('fs');
const path = require('path');

function readData () {
    fs.readFile('./data.json', (err, data) => {
        if(err) {
            console.log(err);
        } else {
            console.log('data:', data);
        }
    })
}

function writeData(writePath, writeData, cb) {
    fs.writeFile(writePath, writeData, (error) => {
        if(error) {
            console.log(error);
        } else {
            cb();
        }
    })
}

function retrieveGivenIdInfo(data) {
    const toInclude = [2, 13, 23];
    const requiredEmployeeInfo = data.employees.filter((emp) => {
        return toInclude.includes(emp.id);
    });
    return requiredEmployeeInfo;
}

function groupByCompanies (data) {
    const companyMap = { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []};
    const groupedEmployees = data.employees.reduce((acc, value) => {
        const company = value.company;
        if(!acc[company]) {
            acc[company] = [];
        }
        acc[company].push(value);
        return acc;
    }, companyMap); 
    return groupedEmployees;
}

function getDataOfPowerpuffBrigade (data) {
    const requiredData = data.employees.filter((emp) => {
        return emp.company.toLowerCase().includes('powerpuff brigade');
    })
    return requiredData;
}

function removeEntryFromData (data) {
    const newEmployees = data.employees.filter((emp) => {
        return emp.id !== 2;
    })
    return {newEmployees};
}
 
// function sortByCompany (data) {
//     const sortedData = data.employees.sort((emp1, emp2) => {
//         if(emp1.company > emp2.company) {
//             return -1;
//         } else if(emp1.company < emp2.co) {
//             return 1;
//         } else {
//             if(emp1.id > emp2.id) {
//                 return -1;
//             } ele
//         }
//     })
//     return sortedData;
// }
// console.log(sortByCompany(data));
